package com.workitman.ben.plane;

import android.content.Context;
import android.util.AttributeSet;

import static com.workitman.ben.plane.Commander.MOTOR_CHANNEL;
import static com.workitman.ben.plane.Commander.RUDDER_CHANNEL;

public class ControllerLeftView extends ControllerView {

  public ControllerLeftView(Context context) {
    super(context);
  }

  public ControllerLeftView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerLeftView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean isRepositionX() {
    return false;
  }

  @Override
  protected boolean isRepositionY() {
    return false;
  }

  @Override
  protected boolean showsText() {
    return true;
  }

  @Override
  protected String getTitle() {
    return "left";
  }

  @Override
  protected float calculateStartY() {
    return getHeight() - getStickRadius();
  }

  @Override
  protected char getVerticalChannel() {
    return MOTOR_CHANNEL;
  }

  @Override
  protected char[] getHorizontalChannel() {
    return new char[]{RUDDER_CHANNEL};
  }

}
