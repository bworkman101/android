package com.workitman.ben.plane;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BlueBroadcastReceiver extends BroadcastReceiver {

    private final BluetoothActivity bluetoothActivity;

    public BlueBroadcastReceiver(BluetoothActivity bluetoothActivity) {
        this.bluetoothActivity = bluetoothActivity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        final String action = intent.getAction();

        switch (action) {
            case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                Log.d("plane bluetooth rec", "ACTION_ACL_DISCONNECTED");
                Log.d("plane bluetooth rec", "attempting to reconnect");
                while(!bluetoothActivity.initBluetoothSocket()) {
                    Log.d("plane bluetooth rec", "failure, retrying");
                }
                break;
        }
    }

}
