package com.workitman.ben.plane;

import java.util.HashMap;
import java.util.Map;

/**
 * Send channel commands to the socket thread.
 */
public class Commander {

    public static final Character MOTOR_CHANNEL = 'm';
    public static final Character ELEVATOR_CHANNEL = 'a';
    public static final Character RUDDER_CHANNEL = 'd';
    public static final Character AILERON_LEFT_CHANNEL = 'b';
    public static final Character AILERON_RIGHT_CHANNEL = 'c';
    private final int CHANGE_DEGREES = 3;
    private final SocketThread socketThread;
    private Map<Character, Integer> oldCommands = new HashMap<>();

    public Commander(SocketThread socketThread) {
        this.socketThread = socketThread;
    }

    public void destroy() {
        if (socketThread != null) {
            socketThread.cancel();
        }
    }

    public void sendCommand(char channel, int degrees) {
        Integer previousDegrees = oldCommands.get(channel);

        // only send command if a change of CHANGE_DEGREES has occured
        if (previousDegrees != null) {
            int diff = Math.abs(degrees - previousDegrees);

            if (diff > CHANGE_DEGREES) {
                interpretAndSendCommand(channel, degrees);
                oldCommands.put(channel, degrees);
            }
        } else {
            interpretAndSendCommand(channel, degrees);
            oldCommands.put(channel, degrees);
        }

    }

    private void interpretAndSendCommand(char channel, int degrees) {
        String command = makeCommandString(channel, degrees);

        if (socketThread != null) {
            socketThread.write(command.getBytes());
        }
    }

    private String makeCommandString(char channel, int degrees) {
        String commandStr = "z" + channel;

        if (degrees < 10) {
            commandStr += "00" + degrees;
        } else if (degrees < 100) {
            commandStr += "0" + degrees;
        } else {
            commandStr += degrees;
        }

        commandStr += "z";
        return commandStr;
    }

}
