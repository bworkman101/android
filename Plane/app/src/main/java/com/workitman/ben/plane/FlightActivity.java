package com.workitman.ben.plane;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class FlightActivity extends AppCompatActivity implements BluetoothActivity {

    private Commander commander;
    private JoyStickControl joyStickControl;
    private BlueBroadcastReceiver blueBroadcastReceiver = new BlueBroadcastReceiver(this);
    private BluetoothDevice bluetoothDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        Log.e("plane bluetooth", "created");
    }

    @Override
    protected void onDestroy() {
        if (commander != null) {
            commander.destroy();
        }
        unregisterReceiver(blueBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.flight, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_connect_plane) {
            BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();

            RadioGroup blueListRadioGroup = (RadioGroup) findViewById(R.id.blueListRadioGroup);
            blueListRadioGroup.removeAllViews();

            for (BluetoothDevice device : devices) {
                RadioButton rb = new BlueSelectButton(getApplicationContext(), device.getAddress());
                rb.setText(device.getName() + " [" + device.getAddress() + "]");
                rb.setTextColor(Color.BLACK);
                blueListRadioGroup.addView(rb);
            }

            blueListRadioGroup.invalidate();

            View controllerLayout = findViewById(R.id.controllerLayout);
            controllerLayout.setVisibility(View.GONE);

            View blueLayout = findViewById(R.id.scanBlueToothLayout);
            blueLayout.setVisibility(View.VISIBLE);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public boolean dispatchKeyEvent(KeyEvent ev) {
//        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//            if (ev.getKeyCode() == KeyEvent.KEYCODE_BUTTON_R1) {
//                if (joyStickControl != null) {
//                    joyStickControl.armMotor();
//                }
//            }
//        }
//        return super.dispatchKeyEvent(ev);
//    }

    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            if (joyStickControl != null) {
                float hL = ev.getAxisValue(MotionEvent.AXIS_X); // value -1 to 1
                float hR = ev.getAxisValue(MotionEvent.AXIS_Z); // value -1 to 1
                float vR = ev.getAxisValue(MotionEvent.AXIS_RZ); // value -1 to 1
                float throttle = ev.getAxisValue(MotionEvent.AXIS_GAS); // value 0 to 1
                joyStickControl.sendAxisEvents(hL, throttle, hR, vR);
            }
        }
        return super.dispatchGenericMotionEvent(ev);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_flight, container, false);
            return rootView;
        }
    }

    public void blueToothSelected(View view) {

        RadioGroup blueListRadioGroup = (RadioGroup) findViewById(R.id.blueListRadioGroup);
        BlueSelectButton selectButton = (BlueSelectButton) findViewById(blueListRadioGroup.getCheckedRadioButtonId());
        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();
        bluetoothDevice = bluetoothAdapter.getRemoteDevice(selectButton.getBlueAddress());

        initBlueBroadcastReciever();
        initBluetoothSocket();
    }

    /**
     * Initialize the selected bluetooth device.
     * return true is success, false if not
     *
     * @return
     */
    @Override
    public boolean initBluetoothSocket() {
        try {
            BluetoothSocket socket = bluetoothDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            socket.connect();
            SocketThread socketThread = new SocketThread(socket);
            socketThread.start();
            commander = new Commander(socketThread);
            joyStickControl = new JoyStickControl(commander);

            View controllerLayout = findViewById(R.id.controllerLayout);
            controllerLayout.setVisibility(View.VISIBLE);
            View blueLayout = findViewById(R.id.scanBlueToothLayout);
            blueLayout.setVisibility(View.GONE);
            return true;
        }
        catch (IOException e) {
            Log.e("plane bluetooth", "unable to connect to socket", e);
            return false;
        }
    }

    private BluetoothAdapter getBluetoothAdapter() {
        Object blueSysServ = getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = null;

        if (blueSysServ instanceof BluetoothManager) {
            BluetoothManager bluetoothManager = (BluetoothManager) blueSysServ;
            bluetoothAdapter = bluetoothManager.getAdapter();
        } else if (blueSysServ instanceof BluetoothAdapter) {
            bluetoothAdapter = (BluetoothAdapter) blueSysServ;
        }

        if (bluetoothAdapter == null) {
            throw new AndroidRuntimeException("bluetooth service is not a BluetoothAdapter or a BluetoothManager, it's a " + blueSysServ.getClass().getName());
        }
        return bluetoothAdapter;
    }

    private void initBlueBroadcastReciever() {
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(blueBroadcastReceiver, filter);
    }

    public Commander getCommander() {
        return commander;
    }
}
