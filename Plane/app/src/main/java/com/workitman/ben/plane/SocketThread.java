package com.workitman.ben.plane;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SocketThread extends Thread {

  private final BluetoothSocket mmSocket;
  private final InputStream mmInStream;
  private final OutputStream mmOutStream;

  public SocketThread(BluetoothSocket socket) {
    mmSocket = socket;
    BufferedInputStream tmpIn = null;
    OutputStream tmpOut = null;

    // Get the input and output streams, using temp objects because
    // member streams are final
    try {
      tmpIn = new BufferedInputStream(socket.getInputStream());
      tmpOut = socket.getOutputStream();
    } catch (IOException e) {
      Log.d("plane bluetooth", "socket problems", e);
    }

    mmInStream = tmpIn;
    mmOutStream = tmpOut;
  }

  public void run() {
    // Keep listening to the InputStream until an exception occurs
    while (true) {
      try {
        int ch = -1;
        StringBuffer msg = new StringBuffer();
        while ((ch = mmInStream.read()) > 0) {
          msg.append((char) ch);
        }
        Log.d("plane bluetooth", "from bluetooth " + msg);
      } catch (IOException e) {
        break;
      }
    }
  }

  /* Call this from the main activity to send data to the remote device */
  public void write(byte[] bytes) {
    try {
      mmOutStream.write(bytes);
      mmOutStream.flush();
    } catch (IOException e) {
      Log.d("plane bluetooth", "socket write problems", e);
    }
  }

  /* Call this from the main activity to shutdown the connection */
  public void cancel() {
    try {
      mmSocket.close();
    } catch (IOException e) { }
  }
}
