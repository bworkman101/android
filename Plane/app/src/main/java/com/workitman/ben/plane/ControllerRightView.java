package com.workitman.ben.plane;

import android.content.Context;
import android.util.AttributeSet;

import static com.workitman.ben.plane.Commander.AILERON_LEFT_CHANNEL;
import static com.workitman.ben.plane.Commander.AILERON_RIGHT_CHANNEL;
import static com.workitman.ben.plane.Commander.ELEVATOR_CHANNEL;

public class ControllerRightView extends ControllerView  {

  public ControllerRightView(Context context) {
    super(context);
  }

  public ControllerRightView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerRightView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean isRepositionX() {
    return false;
  }

  @Override
  protected boolean isRepositionY() {
    return false;
  }

  @Override
  protected boolean showsText() {
    return false;
  }

  @Override
  protected char getVerticalChannel() {
    return ELEVATOR_CHANNEL;
  }

  @Override
  protected char[] getHorizontalChannel() {
    return new char[]{AILERON_LEFT_CHANNEL, AILERON_RIGHT_CHANNEL};
  }

  @Override
  protected String getTitle() {
    return "right";
  }

}
