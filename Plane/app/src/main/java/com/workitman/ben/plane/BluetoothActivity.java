package com.workitman.ben.plane;

import android.bluetooth.BluetoothDevice;

public interface BluetoothActivity {

    /**
     * Initialize the selected bluetooth device.
     * return true is success, false if not
     *
     * @return
     */
    boolean initBluetoothSocket();

}
