package com.workitman.ben.plane;

import android.content.Context;
import android.widget.RadioButton;

public class BlueSelectButton extends RadioButton {

  private final String blueAddress;

  public BlueSelectButton(Context context, String blueAddress) {
    super(context);
    this.blueAddress = blueAddress;
  }

  public String getBlueAddress() {
    return blueAddress;
  }

}
