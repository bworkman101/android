package com.workitman.ben.plane;

import android.util.Log;

public class JoyStickControl {

//    public static final int ARMED_DEGREES = 45;
    private final Commander commander;
//    private Boolean motorArmed = false;

    public JoyStickControl(Commander commander) {
        this.commander = commander;
    }

    public void sendAxisEvents(float horizontalLeft,
                               float trigger,
                               float horizontalRight,
                               float verticalRight) {
        int rudderDegrees = toDegrees(horizontalRight, 75);
        int aileronDegrees = toDegrees(horizontalLeft);
        int elevatorDegrees = toDegrees(verticalRight, 85);
        commander.sendCommand(Commander.ELEVATOR_CHANNEL, elevatorDegrees);
        commander.sendCommand(Commander.RUDDER_CHANNEL, rudderDegrees);
        commander.sendCommand(Commander.AILERON_LEFT_CHANNEL, aileronDegrees);
        commander.sendCommand(Commander.AILERON_RIGHT_CHANNEL, aileronDegrees);

        float motorDegrees = 180 * trigger;
        Log.d("plane bluetooth", "motor degrees " + motorDegrees);
        commander.sendCommand(Commander.MOTOR_CHANNEL, (int) motorDegrees);
    }

    /**
     * Convert postition of -1 to 1 to degrees 0 to 180
     *
     * @param position Position is a value between -1.0 and 1.0
     * @return Postion to degrees between 0 and 180
     */
    public static int toDegrees(float position) {
        float degrees = 90f + position * 90f;
        return (int) degrees;
    }

    /**
     * Convert postition of -1 to 1 to degrees from 0 to (bound * 2)
     *
     * @param position
     * @param bound
     * @return
     */
    public static int toDegrees(float position, float bound) {
        float degrees = bound + position * bound;
        return (int) degrees;
    }

//    /**
//     * Toggle motor armed on and off
//     */
//    public void armMotor() {
//        synchronized (motorArmed) {
//            if (motorArmed) {
//                motorArmed = false;
//                commander.sendCommand(Commander.MOTOR_CHANNEL, 1);
//                Log.d("plane bluetooth", "motor dis-armed");
//            } else {
//                motorArmed = true;
//                commander.sendCommand(Commander.MOTOR_CHANNEL, ARMED_DEGREES);
//                Log.d("plane bluetooth", "motor armed");
//            }
//        }
//    }
}
