package com.workitman.plane;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by ben on 5/12/14.
 */
public abstract class ControllerView extends View {

  private float x = -1;
  private float y = -1;
  private int verticalDegrees;
  private int horizontalDegrees;
  private float textSize = -1;

  public ControllerView(Context context) {
    super(context);
  }

  public ControllerView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  protected abstract String getTitle();

  protected abstract boolean isRepositionX();

  protected abstract boolean isRepositionY();

  protected abstract boolean showsText();

  protected abstract char getVerticalChannel();

  protected abstract char[] getHorizontalChannel();

  protected int offestHorizontal() {
    return 1;
  }

  protected float calculateStartX() {
    return getWidth() / 2;
  }

  protected float calculateStartY() {
    return getHeight() / 2;
  }

  protected float getStickRadius() {
    return (float) (getHeight() * .10);
  }

  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    textSize = (float) (getHeight() * .05);
    super.onSizeChanged(w, h, oldw, oldh);
  }

  @Override
  protected void onDraw(Canvas canvas) {

    if (x == -1) {
      x = calculateStartX();
      y = calculateStartY();
    }

    {
      Paint paint = new Paint();
      paint.setColor(Color.BLACK);
      paint.setStrokeWidth(2);
      canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2, paint);
      canvas.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight(), paint);
    }
    {
      Paint paint = new Paint();
      paint.setColor(Color.GREEN);
      canvas.drawCircle(x, y, getStickRadius(), paint);
    }
    {
      Paint paint = new Paint();
      paint.setColor(Color.BLACK);
      paint.setTextSize(textSize);
      String msg = String.format("x=%1$s degX=%2$s  y=%3$s degY=%4$s", (int) x, horizontalDegrees, (int) y, verticalDegrees);
      canvas.drawText(msg, 0, textSize + 5, paint);
    }
    if (showsText()) {
      Paint paint = new Paint();
      paint.setColor(Color.BLACK);
      paint.setTextSize(textSize);
      if (hasSocket()) {
        canvas.drawText("connected", 0, getHeight() - 50, paint);
      } else {
        canvas.drawText("not connected", 0, getHeight() - 50, paint);
      }
    }
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    switch (event.getAction()) {
      case MotionEvent.ACTION_MOVE:
        return moveStick(event);
      case MotionEvent.ACTION_DOWN:
        return moveStick(event);
      case MotionEvent.ACTION_UP:
        if (isRepositionX()) {
          x = calculateStartX();
        }
        if (isRepositionY()) {
          y = calculateStartY();
        }
        return true;
      default:
        return super.onTouchEvent(event);
    }
  }

  private boolean moveStick(MotionEvent event) {
    float eventX = event.getX();
    float eventY = event.getY();
    if (eventX < (getWidth() - getStickRadius())
            && eventX > getStickRadius()
            && eventY < (getHeight() - getStickRadius())
            && eventY > getStickRadius()) {

      x = eventX;
      y = eventY;

      horizontalDegrees = calculateDegrees(getWidth(), x);
      verticalDegrees = calculateDegrees(getHeight(), getHeight() - y);

      if (hasSocket()) {
        for (char channel : getHorizontalChannel()) {
          writeSocketServoCmd(horizontalDegrees * offestHorizontal(), channel);
        }
        writeSocketServoCmd(verticalDegrees, getVerticalChannel());
      }

      invalidate();

      return true;
    } else {
      return false;
    }
  }

  private int calculateDegrees(float screenWidth, float position) {
    float stickRadius = getStickRadius();
    float realWidth = (screenWidth - (stickRadius * 2));
    float realPosition = position - stickRadius;
    return (int) (180 * (realPosition / realWidth));
  }

  private void writeSocketServoCmd(int degrees, char channel) {

    String command = "z" + channel;
    if (degrees < 10) {
      command += "00" + degrees;
    } else if (degrees < 100) {
      command += "0" + degrees;
    } else {
      command += degrees;
    }
    command += "z";

    Log.d("plane bluetooth", "command to socket " + command);

    SocketThread socket = getSocket();

    socket.write(command.getBytes());

    Log.d("plane bluetooth", "command sent");

  }

  private SocketThread getSocket() {
    FlightActivity activity = (FlightActivity) getContext();
    return activity.getSocketThread();
  }

  private boolean hasSocket() {
    return getSocket() != null;
  }
}
