package com.workitman.plane;

import android.content.Context;
import android.widget.RadioButton;

/**
 * Created by ben on 5/12/14.
 */
public class BlueSelectButton extends RadioButton {

  private final String blueAddress;

  public BlueSelectButton(Context context, String blueAddress) {
    super(context);
    this.blueAddress = blueAddress;
  }

  public String getBlueAddress() {
    return blueAddress;
  }

}
