package com.workitman.plane;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ben on 5/8/14.
 */
public class ControllerRightView extends ControllerView  {

  public ControllerRightView(Context context) {
    super(context);
  }

  public ControllerRightView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerRightView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean isRepositionX() {
    return false;
  }

  @Override
  protected boolean isRepositionY() {
    return false;
  }

  @Override
  protected boolean showsText() {
    return false;
  }

  @Override
  protected char getVerticalChannel() {
    return 'a';
  }

  @Override
  protected char[] getHorizontalChannel() {
    return new char[]{'b', 'c'};
  }

  @Override
  protected String getTitle() {
    return "right";
  }

}
