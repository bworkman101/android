package com.workitman.plane;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ben on 5/8/14.
 */
public class ControllerLeftView extends ControllerView {

  public ControllerLeftView(Context context) {
    super(context);
  }

  public ControllerLeftView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerLeftView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean isRepositionX() {
    return false;
  }

  @Override
  protected boolean isRepositionY() {
    return false;
  }

  @Override
  protected boolean showsText() {
    return true;
  }

  @Override
  protected String getTitle() {
    return "left";
  }

  @Override
  protected float calculateStartY() {
    return getHeight() - getStickRadius();
  }

  @Override
  protected char getVerticalChannel() {
    return 'm';
  }

  @Override
  protected char[] getHorizontalChannel() {
    return new char[]{'d'};
  }

}
