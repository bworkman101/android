package com.workitman.plane;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.AndroidException;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class FlightActivity extends ActionBarActivity {

  private SocketThread socketThread;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_flight);

    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
              .add(R.id.container, new PlaceholderFragment())
              .commit();
    }
  }

  @Override
  protected void onDestroy() {
    ControllerView controllerLeftView = (ControllerView) findViewById(R.id.controllerLeftView);
    if (socketThread != null) {
      socketThread.cancel();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {

    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.flight, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    if (id == R.id.action_connect_plane) {
      BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

      Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();

      RadioGroup blueListRadioGroup = (RadioGroup) findViewById(R.id.blueListRadioGroup);
      blueListRadioGroup.removeAllViews();

      for (BluetoothDevice device : devices) {
        RadioButton rb = new BlueSelectButton(getApplicationContext(), device.getAddress());
        rb.setText(device.getName() + " [" + device.getAddress() + "]");
        rb.setTextColor(Color.BLACK);
        blueListRadioGroup.addView(rb);
      }

      blueListRadioGroup.invalidate();

      View controllerLayout = findViewById(R.id.controllerLayout);
      controllerLayout.setVisibility(View.GONE);

      View blueLayout = findViewById(R.id.scanBlueToothLayout);
      blueLayout.setVisibility(View.VISIBLE);

      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  private BluetoothAdapter getBluetoothAdapter() {
    Object blueSysServ = getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
    BluetoothAdapter bluetoothAdapter = null;

    if (blueSysServ instanceof BluetoothManager) {
      BluetoothManager bluetoothManager = (BluetoothManager) blueSysServ;
      bluetoothAdapter = bluetoothManager.getAdapter();
    }
    else if (blueSysServ instanceof BluetoothAdapter) {
      bluetoothAdapter = (BluetoothAdapter) blueSysServ;
    }

    if (bluetoothAdapter == null) {
      throw new AndroidRuntimeException("bluetooth service is not a BluetoothAdapter or a BluetoothManager, it's a " + blueSysServ.getClass().getName());
    }
    return bluetoothAdapter;
  }

  /**
   * A placeholder fragment containing a simple view.
   */
  public static class PlaceholderFragment extends Fragment {

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View rootView = inflater.inflate(R.layout.fragment_flight, container, false);
      return rootView;
    }
  }

  public void blueToothSelected(View view) {

    RadioGroup blueListRadioGroup = (RadioGroup) findViewById(R.id.blueListRadioGroup);
    BlueSelectButton selectButton = (BlueSelectButton) findViewById(blueListRadioGroup.getCheckedRadioButtonId());
    BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();
    BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(selectButton.getBlueAddress());

    try {
      BluetoothSocket socket = bluetoothDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
      socket.connect();
      this.socketThread = new SocketThread(socket);
      this.socketThread.start();

      View controllerLayout = findViewById(R.id.controllerLayout);
      controllerLayout.setVisibility(View.VISIBLE);
      View blueLayout = findViewById(R.id.scanBlueToothLayout);
      blueLayout.setVisibility(View.GONE);
    } catch (IOException e) {
      Log.e("plane bluetooth", "unable to connect to socket", e);
    }
  }

  public SocketThread getSocketThread() {
    return socketThread;
  }
}
