package com.workitman.ben.discolight;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private final static int REQUEST_ENABLE_BT = 1;

    private SocketThread socketThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        ProgressBar blueProgress = (ProgressBar) findViewById(R.id.blueProgress);
        blueProgress.setVisibility(View.VISIBLE);

        TextView blueText = (TextView) findViewById(R.id.blueText);
        blueText.setText("Connecting Bluetooth");

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    @Override
    protected void onDestroy() {
        if (socketThread != null) {
            socketThread.close();
        }
        super.onDestroy();
    }

    public void colorBtnClick(View view) {
        switch (view.getId()) {
            case R.id.greenBtn:
                socketThread.write("zgz".getBytes());
                break;
            case R.id.redBtn:
                socketThread.write("zrz".getBytes());
                break;
            case R.id.yellowBtn:
                socketThread.write("zyz".getBytes());
                break;
            case R.id.onOff:
                Button button = (Button) view;
                if (button.getText().equals(getString(R.string.turnLightOn))) {
                    button.setText(getString(R.string.turnLightOff));
                    socketThread.write("zoz".getBytes());
                }
                else {
                    button.setText(getString(R.string.turnLightOn));
                    socketThread.write("zfz".getBytes());
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_ENABLE_BT == requestCode && resultCode == RESULT_OK) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : devices) {
                Log.d("bluetooth", device.getName() + " bt device seen");
                if (device.getName().equals("disco-7AB6")) {
                    try {
                        Log.d("bluetooth", "bluetooth connected");
                        BluetoothSocket socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                        socket.connect();
                        this.socketThread = new SocketThread(socket);
                        this.socketThread.start();

                        ProgressBar blueProgress = (ProgressBar) findViewById(R.id.blueProgress);
                        blueProgress.setVisibility(View.INVISIBLE);

                        TextView blueText = (TextView) findViewById(R.id.blueText);
                        blueText.setText("");

                    } catch (IOException e) {
                        Log.e("bluetooth", "unable to connect to socket", e);
                        new AlertDialog.Builder(this)
                                .setTitle("Unable to Connect")
                                .setMessage("Unable to connect to BlueTooth!\n" +
                                        "Make sure the device is running.")
                                .setNeutralButton("Close", null)
                                .setIcon(R.mipmap.crash)
                                .show();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
