package com.workitman.lego;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by ben on 5/8/14.
 */
public class ControllerLeftView extends ControllerView {

  public ControllerLeftView(Context context) {
    super(context);
  }

  public ControllerLeftView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerLeftView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  protected boolean showsText() {
    return true;
  }

//  @Override
//  protected float calculateStartY() {
//    return getHeight() - getStickRadius();
//  }

  @Override
  int verticalDegress(int degrees)
  {
    return (int) ((degrees) * 2.833333333);
  }

  @Override
  protected char getVerticalChannel() {
    return 'm';
  }

  @Override
  protected char getHorizontalChannel() {
    return ' ';
  }

  @Override
  protected boolean isHorizontalSupported() {
    return false;
  }

  @Override
  protected boolean isVerticalSupported() {
    return true;
  }

}
