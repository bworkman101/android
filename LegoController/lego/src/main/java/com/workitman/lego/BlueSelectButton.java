package com.workitman.lego;

import android.content.Context;
import android.widget.RadioButton;

/**
 * Created by ben on 5/12/14.
 */
public class BlueSelectButton extends RadioButton {

  private final String blueAddress;
  private final String blueName;

  public BlueSelectButton(Context context, String blueAddress, String blueName) {
    super(context);
    this.blueAddress = blueAddress;
    this.blueName = blueName;
  }

  public String getBlueAddress() {
    return blueAddress;
  }

  public String getBlueName() {
    return blueName;
  }
}
