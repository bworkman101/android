package com.workitman.lego;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class Activity extends AppCompatActivity {

    private SocketThread socketThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        ControllerView controllerLeftView = (ControllerView) findViewById(R.id.controllerLeftView);
        if (socketThread != null) {
            socketThread.cancel();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.flight, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String legoId = sharedPref.getString(getString(R.string.lego_bt_id), null);

        if (legoId != null)
        {
            findViewById(R.id.controllerLayout).setVisibility(View.GONE);

            TextView selectedBlueText = (TextView) findViewById(R.id.selectedBlueText);
            selectedBlueText.setVisibility(View.VISIBLE);
            String legoName = sharedPref.getString(getString(R.string.lego_bt_name), null);
            selectedBlueText.setText("Lego selected " + legoName + " [" + legoId + "]");

            findViewById(R.id.blueListRadioGroup).setVisibility(View.GONE);
            findViewById(R.id.blueListSelect).setVisibility(View.GONE);
            findViewById(R.id.forgetLego).setVisibility(View.VISIBLE);
            findViewById(R.id.scanBlueToothLayout).setVisibility(View.VISIBLE);

            return super.onOptionsItemSelected(item);
        } else
        {
            return promptForLego(item);
        }
    }

    private boolean promptForLego(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_connect_lego) {

            BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();

            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();

            RadioGroup blueListRadioGroup = (RadioGroup) findViewById(R.id.blueListRadioGroup);
            blueListRadioGroup.removeAllViews();

            for (BluetoothDevice device : devices) {
                RadioButton rb = new BlueSelectButton(getApplicationContext(), device.getAddress(), device.getName());
                rb.setText(device.getName() + " [" + device.getAddress() + "]");
                rb.setTextColor(Color.BLACK);
                blueListRadioGroup.addView(rb);
            }

            blueListRadioGroup.invalidate();

            findViewById(R.id.controllerLayout).setVisibility(View.GONE);
            findViewById(R.id.selectedBlueText).setVisibility(View.GONE);
            findViewById(R.id.blueListRadioGroup).setVisibility(View.VISIBLE);
            findViewById(R.id.blueListSelect).setVisibility(View.VISIBLE);
            findViewById(R.id.scanBlueToothLayout).setVisibility(View.VISIBLE);

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private BluetoothAdapter getBluetoothAdapter() {
        Object blueSysServ = getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = null;

        if (blueSysServ instanceof BluetoothManager) {
            BluetoothManager bluetoothManager = (BluetoothManager) blueSysServ;
            bluetoothAdapter = bluetoothManager.getAdapter();
        } else if (blueSysServ instanceof BluetoothAdapter) {
            bluetoothAdapter = (BluetoothAdapter) blueSysServ;
        }

        if (bluetoothAdapter == null) {
            throw new AndroidRuntimeException("bluetooth service is not a BluetoothAdapter or a BluetoothManager, it's a " + blueSysServ.getClass().getName());
        }
        return bluetoothAdapter;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_flight, container, false);
            return rootView;
        }
    }

    public void blueToothSelected(View view) {

        RadioGroup blueListRadioGroup = (RadioGroup) findViewById(R.id.blueListRadioGroup);
        BlueSelectButton selectButton = (BlueSelectButton) findViewById(blueListRadioGroup.getCheckedRadioButtonId());
        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();
        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(selectButton.getBlueAddress());

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        sharedPref.edit().putString(getString(R.string.lego_bt_id), selectButton.getBlueAddress());
        sharedPref.edit().putString(getString(R.string.lego_bt_name), selectButton.getBlueName());

        try {
            BluetoothSocket socket = bluetoothDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            socket.connect();
            this.socketThread = new SocketThread(socket);
            this.socketThread.start();

            View controllerLayout = findViewById(R.id.controllerLayout);
            controllerLayout.setVisibility(View.VISIBLE);
            View blueLayout = findViewById(R.id.scanBlueToothLayout);
            blueLayout.setVisibility(View.GONE);
        } catch (IOException e) {
            Log.e("lego bluetooth", "unable to connect to socket", e);
            new AlertDialog.Builder(this)
                    .setTitle("Unable to Connect")
                    .setMessage("Unable to connect to BlueTooth!\n" +
                            "Make sure the device is running.")
                    .setNeutralButton("Close", null)
                    .setIcon(R.drawable.crash)
                    .show();
        }
    }

    public void forgetLego(View view) {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        sharedPref.edit().putString(getString(R.string.lego_bt_id), null);
        sharedPref.edit().putString(getString(R.string.lego_bt_name), null);

        findViewById(R.id.controllerLayout).setVisibility(View.VISIBLE);
        findViewById(R.id.scanBlueToothLayout).setVisibility(View.GONE);
    }

    public SocketThread getSocketThread() {
        return socketThread;
    }
}
