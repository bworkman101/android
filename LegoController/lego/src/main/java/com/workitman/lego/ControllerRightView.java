package com.workitman.lego;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by ben on 5/8/14.
 */
public class ControllerRightView extends ControllerView  {

  public ControllerRightView(Context context) {
    super(context);
  }

  public ControllerRightView(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ControllerRightView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @Override
  int horizontalDegress(int degrees)
  {
    return (int) ((1f / 4f) * degrees + 67.5);
  }

  @Override
  protected boolean showsText() {
    return false;
  }

  @Override
  protected char getVerticalChannel() {
    return ' ';
  }

  @Override
  protected char getHorizontalChannel() {
    return 'b';
  }

  @Override
  protected boolean isHorizontalSupported() {
    return true;
  }

  @Override
  protected boolean isVerticalSupported() {
    return false;
  }

}
